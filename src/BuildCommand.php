<?php

namespace Cwd\Docker\Php;

use Composer\Semver\Comparator;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Build command.
 *
 * @author Ludwig Ruderstaller <lr@cwd.at>
 */
class BuildCommand extends Command
{
    const NAME = 'build';
    
    const HUBURL = 'https://registry.hub.docker.com/v2/repositories/library/php/tags/';
    
    /**
     * Twig renderer.
     *
     * @var \Twig_Environment
     */
    protected $twig;
    
    /**
     * @var array
     */
    protected $versions = [];
    
    private $xdebugVersion = null;
    
    /*
    protected $version = [
        '5.6',
        '7.0',
        '7.1',
        '7.2',
    ];

    protected $amount = [
        '5.6' => 1,
        '7.0' => 1,
        '7.1' => 5,
        '7.2' => 5,
    ];

    protected $ignore = [
        'zts',
        'apache',
    ];
    */
    
    /**
     * BuildCommand constructor.
     *
     * @param \Twig_Environment $twig
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(\Twig_Environment $twig, $versions)
    {
        parent::__construct(static::NAME);
        
        $this->twig = $twig;
        $this->versions = $versions;
    }
    
    /**
     * {@inheritdoc}
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws RuntimeException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $distDir = getcwd() . '/' . rtrim($input->getOption('dir'), '/');
        
        
        if (is_dir($distDir)) {
            $this->recursiveRemove($distDir);
        }
        foreach ($this->versions as $type => $versions) {
            $template = sprintf('Dockerfile-%s.twig', $type);
            
            foreach ($versions as $version => $versionData) {
                $this->createFile($version, $versionData, $template, $type, $distDir);
                
                if ($versionData['use_xdebug']) {
                    $this->createFile($version, $versionData, 'Dockerfile-xdebug.twig', $type, $distDir, true);
                }
            }
        }
        
        file_put_contents('.gitlab-ci.yml', $this->twig->render(
            'gitlab-ci.twig',
            [
                'images' => $this->versions,
            ]
        ));
    }
    
    private function recursiveRemove(string $path)
    {
        foreach (glob($path . '/*') as $file) {
            is_dir($file) ? $this->recursiveRemove($file) : unlink($file);
        }
        
        rmdir($path);
    }
    
    private function createFile($version, $versionData, $template, $type, $distDir, $isXdebug = false)
    {
        $isPHP80 = (bool)Comparator::greaterThan($versionData['tags'][0], '7.4.99');
        $isPHP74 = Comparator::greaterThan($versionData['tags'][0], '7.3.99') && Comparator::lessThan($versionData['tags'][0], '8.0.0');
        $isPHP73 = Comparator::greaterThan($versionData['tags'][0], '7.2.99') && Comparator::lessThan($versionData['tags'][0], '7.4.0');
        $isPHP72 = Comparator::greaterThan($versionData['tags'][0], '7.1.99') && Comparator::lessThan($versionData['tags'][0], '7.3.0');
        $isPHP71 = Comparator::greaterThan($versionData['tags'][0], '7.0.99') && Comparator::lessThan($versionData['tags'][0], '7.2.0');
        $isPHP56 = (bool)Comparator::lessThan($versionData['tags'][0], '5.7.99');
        
        $versionDir = $distDir . '/' . $version . '/' . $type;
        if ($isXdebug) {
            $versionDir .= '-xdebug';
        }
        if (!mkdir($versionDir, 0755, true) && !is_dir($versionDir)) {
            throw new RuntimeException(sprintf('Not possible to create "%s" directory', $versionDir));
        }
        
        $xdebugVersion = ($isPHP56) ? '2.5.5' : $this->getXdebugVersion();
        
        #	copy('mongodb.patch', $versionDir.'/mongodb.patch');
        file_put_contents($versionDir . '/Dockerfile', $this->twig->render(
            $template,
            [
                'isPHP56' => $isPHP56,
                'isPHP71' => $isPHP71,
                'isPHP72' => $isPHP72,
                'isPHP73' => $isPHP73,
                'isPHP74' => $isPHP74,
                'isPHP80' => $isPHP80,
                'docker_image' => $versionData['image'],
                'xdebug_version' => $xdebugVersion,
                'version' => $version,
                'realVersion' => $versionData['tags'][0],
                'type' => $type,
            ]
        ));
    }
    
    private function getXdebugVersion(): string
    {
        if ($this->xdebugVersion === null) {
            $xDebugPackage = $this->findLatestXdebugPackage();
            $this->xdebugVersion = explode('-', $xDebugPackage)[1];
        }
        
        return $this->xdebugVersion;
    }
    
    private function findLatestXdebugPackage(): string
    {
        $rss = simplexml_load_string(file_get_contents('https://pecl.php.net/feeds/pkg_xdebug.rss'));
        
        return str_replace(' ', '-', $rss->item[0]->title);
    }
    
    /**
     * {@inheritdoc}
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \RuntimeException
     * @deprecated WIP
     */
    public function grabVersionsExecute(InputInterface $input, OutputInterface $output)
    {
        $distDir = getcwd() . '/' . rtrim($input->getOption('dir'), '/');
        $xdebugVersion = $this->getXdebugVersion();
        
        $versions = $this->getPackageVersions();
        $intresting = [];
        
        foreach ($versions as $version) {
            if (
                (count(array_filter($this->ignore, create_function('$e', 'return strstr("' . $version->name . '", $e);'))) == 0) &&
                (count(array_filter($this->version, create_function('$e', 'return strstr("' . $version->name . '", $e);'))) > 0) &&
                strstr($version->name, 'alpine')
            ) {
                $type = (strstr($version->name, 'fpm')) ? 'fpm' : 'cli';
                $versionString = str_replace(['-alpine', '-fpm'], '', $version->name);
                
                if (Comparator::greaterThan($versionString, '7.1.99')) {
                    $major = '7.2';
                } else {
                    if (Comparator::greaterThan($versionString, '7.0.99')) {
                        if (strstr($versionString, 'RC')) {
                            continue;
                        }
                        $major = '7.1';
                    } else {
                        if (Comparator::greaterThan($versionString, '6.9.99')) {
                            $major = '7.0';
                        } else {
                            if (Comparator::greaterThan($versionString, '5.6.28')) {
                                $major = '5.6';
                            } else {
                                continue;
                            }
                        }
                    }
                }
                
                dump([count($intresting[$type][$major]), $this->amount[$major]]);
                
                if (count($intresting[$type][$major]) >= $this->amount[$major]) {
                    continue;
                }
                
                $intresting[$type][$major][$versionString] = $version;
            }
        }
        
        
        dump($intresting);
        
        return;
        
        
        if (is_dir($distDir)) {
            $this->recursiveRemove($distDir);
        }
        
        $output->writeln('<info>Docker images scaffold done</info>');
        $output->writeln('');
    }
    
    private function getPackageVersions()
    {
        return json_decode($this->tmp);
        
        
        $next = self::HUBURL;
        while (($next = $this->loadTagData($next))) {
            ;
        }
    }
    
    private function loadTagData($url)
    {
        $rawData = json_decode(file_get_contents($url));
        
        foreach ($rawData->results as $result) {
            $this->versions[] = $result;
        }
        
        return ($rawData->next != '') ? $rawData->next : false;
        
    }
    
    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName(static::NAME)
            ->setDescription('Build Dockerfile for different Versions')
            ->addOption('dir', 'd', InputArgument::OPTIONAL, 'Dist directory', 'dist');
    }
}
